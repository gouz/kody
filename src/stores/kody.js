import { defineStore } from "pinia";

import axios from "axios";

export const useStore = defineStore("kody", {
  state: () => ({
    decks: [],
    scene: null,
    engine: null,
    meshes: {},
  }),
  getters: {
    getDecks(state) {
      return state.decks;
    },
  },
  actions: {
    fetchJSONs() {
      this._fetchDecks();
    },
    async _fetchDecks() {
      try {
        const data = await axios.get(`/json/decks.json`);
        this.decks = data.data.decks;
      } catch (error) {
        console.error(error);
      }
    },
  },
});
