import { MeshBuilder, StandardMaterial, Color3, Axis } from "babylonjs";
import { makeHole, addItem } from "./Tools";
import { config } from "../assets/json/config.json";
import { MeshWriter } from "meshwriter";

export default class Card {
  constructor(scene, name, code, color, icon) {
    this.name = name;
    this.code = code;
    this.color = color;
    this.scene = scene;
    this.material = new StandardMaterial("std", this.scene);
    this.material.diffuseColor = Color3.FromHexString(color);
    this.icon = icon;
  }

  render3D() {
    let card = MeshBuilder.CreateBox(
      "card",
      {
        width: config.card.width,
        height: config.card.height,
        depth: config.card.depth,
      },
      this.scene
    );
    card.position.x = config.card.width / 2;
    card.position.y = config.card.height / 2;
    card.position.z = config.card.depth / 2;
    // holes
    card = makeHole(
      this.scene,
      card,
      config.bracket.size / 2,
      config.bracket.size / 2,
      0,
      config.bracket.size,
      config.bracket.size,
      config.card.depth
    );
    card = makeHole(
      this.scene,
      card,
      config.card.width - config.bracket.size / 2 - config.bracket.size,
      config.bracket.size / 2,
      0,
      config.bracket.size,
      config.bracket.size,
      config.card.depth
    );
    card = makeHole(
      this.scene,
      card,
      config.bracket.size / 2,
      config.card.height - config.bracket.size * 2 - config.bracket.size / 2,
      0,
      config.bracket.size,
      config.bracket.size,
      config.card.depth
    );
    card = makeHole(
      this.scene,
      card,
      config.card.width - config.bracket.size / 2 - config.bracket.size,
      config.card.height - 3 * config.text.block.size - config.bracket.size,
      0,
      config.bracket.size,
      config.bracket.size,
      config.card.depth
    );
    // textareas
    let textarea_width =
      config.card.width -
      2 *
        (config.bracket.size +
          config.bracket.size / 2 +
          2 * config.text.block.size);
    let textarea_height = config.bracket.size * 2;
    // bottom
    card = makeHole(
      this.scene,
      card,
      (config.card.width - textarea_width) / 2,
      config.bracket.size / 2,
      config.card.depth / 2,
      textarea_width,
      config.bracket.size,
      config.card.depth / 2
    );
    // top
    card = makeHole(
      this.scene,
      card,
      (config.card.width - textarea_width) / 2,
      config.card.height - config.bracket.size / 2 - textarea_height,
      config.card.depth / 2,
      textarea_width,
      config.bracket.size * 2,
      config.card.depth / 2
    );
    // code
    let lines = this.code;
    let y =
      config.bracket.size +
      config.text.block.size +
      config.code.nb.y * config.code.hole.size +
      config.code.nb.y -
      config.text.block.size;
    this.code.forEach((line) => {
      let cols = line.split("");
      let x =
        (config.card.width -
          (config.code.nb.x * config.code.hole.size +
            config.code.nb.x -
            config.text.block.size)) /
        2;
      for (let b of cols) {
        if (b == " ")
          card = makeHole(
            this.scene,
            card,
            x,
            y,
            0,
            config.code.hole.size,
            config.code.hole.size,
            config.card.depth
          );
        x += config.code.hole.size + config.text.block.size;
      }
      y -= config.code.hole.size + config.text.block.size;
    });
    // header
    // text
    const Writer = MeshWriter(this.scene, { scale: 1 });
    const text = new Writer(this.name.replace(/_/g, " "), {
      "letter-height": 5,
      "letter-thickness": config.card.depth / 2,
      "font-family": "comic",
      color: this.color,
    });
    const textMesh = text.getMesh();
    const bb = textMesh.getBoundingInfo().boundingBox.maximumWorld;
    textMesh.rotation.x = -Math.PI / 2;
    card = addItem(
      this.scene,
      card,
      textMesh,
      (config.card.width - Number(bb.x)) / 2,
      config.card.height -
        config.bracket.size / 2 -
        textarea_height +
        (textarea_height - Number(bb.z)) / 2,
      config.card.depth / 2
    );

    // kody
    let kody = config.kody.code;
    let kw = kody[0].length;
    let kh = kody.length;
    let kz = config.card.depth / 2;
    let kb = MeshBuilder.CreateBox("k", {
      width: kw,
      height: kh,
      depth: kz,
    });
    kb.position.x = kw / 2;
    kb.position.y = kh / 2;
    kb.position.z = kz / 2;
    let _y = 0;
    kody.forEach((row) => {
      let _x = 0;
      row.forEach((col) => {
        if (!col)
          kb = makeHole(
            this.scene,
            kb,
            _x,
            kh - _y - config.text.block.size,
            0,
            config.text.block.size,
            config.text.block.size,
            kz
          );
        _x++;
      });
      _y++;
    });
    card = addItem(
      this.scene,
      card,
      kb,
      (config.card.width - kw) / 2,
      config.bracket.size / 2 + (config.bracket.size - kh) / 2,
      config.card.depth / 2
    );

    card.material = this.material;
    card.translate(Axis.X, -config.card.width / 2, window.scene);
    card.translate(Axis.Y, -config.card.height / 2, window.scene);
    return card;
  }
}
