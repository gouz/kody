import { MeshBuilder, StandardMaterial, Color3, Axis } from "babylonjs";
import { makeHole, addItem } from "./Tools";
import { config } from "../assets/json/config.json";

export default class DeckBack {
  constructor(scene, nb, color) {
    this.offset = 2 * 2 * (config.deck.thickness + config.print.tolerance.xy);
    this.bracket_size =
      config.bracket.size -
      2 * (config.print.tolerance.xy + config.print.tolerance.xy);
    this.width = config.card.width + this.offset;
    this.height = config.card.height + this.offset;
    this.cardDepth = config.card.depth;
    this.nbCards = nb;
    this.depth =
      (this.cardDepth + config.print.tolerance.z) * this.nbCards +
      config.deck.thickness;
    this.scene = scene;
    this.material = new StandardMaterial("std", this.scene);
    this.material.diffuseColor = Color3.FromHexString(color);
  }

  render3D() {
    let back = MeshBuilder.CreateBox(
      "back",
      {
        width: this.width,
        height: this.height,
        depth: this.depth,
      },
      this.scene
    );
    back.position.x = this.width / 2;
    back.position.y = this.height / 2;
    back.position.z = this.depth / 2;
    // internal
    back = makeHole(
      this.scene,
      back,
      2 * config.deck.thickness,
      2 * config.deck.thickness,
      config.deck.thickness,
      this.width - 4 * config.deck.thickness,
      this.height - 4 * config.deck.thickness,
      this.depth - config.deck.thickness
    );
    // top & bottom
    let w = config.deck.corner * config.deck.thickness;
    back = makeHole(
      this.scene,
      back,
      w,
      0,
      config.deck.thickness,
      this.width - 2 * w,
      this.height,
      this.depth - config.deck.thickness
    );
    back = makeHole(
      this.scene,
      back,
      0,
      0,
      0,
      w,
      config.deck.thickness,
      this.depth
    );
    back = makeHole(
      this.scene,
      back,
      this.width - w,
      0,
      0,
      w,
      config.deck.thickness,
      this.depth
    );
    back = makeHole(
      this.scene,
      back,
      0,
      this.height - config.deck.thickness,
      0,
      w,
      config.deck.thickness,
      this.depth
    );
    back = makeHole(
      this.scene,
      back,
      this.width - w,
      this.height - config.deck.thickness,
      0,
      w,
      config.deck.thickness,
      this.depth
    );
    // left & right
    back = makeHole(
      this.scene,
      back,
      0,
      w,
      config.deck.thickness,
      this.width,
      this.height - 2 * w,
      this.depth - config.deck.thickness
    );
    back = makeHole(
      this.scene,
      back,
      0,
      0,
      0,
      config.deck.thickness,
      w,
      this.depth
    );
    back = makeHole(
      this.scene,
      back,
      this.width - config.deck.thickness,
      0,
      0,
      config.deck.thickness,
      w,
      this.depth
    );
    back = makeHole(
      this.scene,
      back,
      0,
      this.height - w,
      0,
      config.deck.thickness,
      w,
      this.depth
    );
    back = makeHole(
      this.scene,
      back,
      this.width - config.deck.thickness,
      this.height - w,
      0,
      config.deck.thickness,
      w,
      this.depth
    );
    // code hole
    let code_hole_width =
      config.code.hole.size * config.code.nb.x + config.code.nb.x;
    let code_hole_height =
      config.code.hole.size * config.code.nb.y + config.code.nb.y;
    back = makeHole(
      this.scene,
      back,
      (this.width - code_hole_width) / 2,
      config.deck.thickness +
        config.deck.thickness +
        config.bracket.size / 2 +
        config.bracket.size / 2 +
        config.bracket.size,
      0,
      code_hole_width,
      code_hole_height,
      config.deck.thickness
    );
    // kody textarea
    let textarea_width =
      config.card.width -
      2 *
        (config.bracket.size +
          config.bracket.size / 2 +
          2 * config.text.block.size);
    let textarea_height = this.bracket_size;
    back = makeHole(
      this.scene,
      back,
      (this.width - textarea_width) / 2,
      2 * config.deck.thickness +
        config.bracket.size / 2 +
        config.print.tolerance.xy,
      config.deck.thickness / 2,
      textarea_width,
      textarea_height,
      config.deck.thickness / 2
    );
    // kody
    let kody = config.kody.code;
    let kw = kody[0].length * config.text.block.size;
    let kh = kody.length * config.text.block.size;
    let kz = config.deck.thickness / 2;
    let kb = MeshBuilder.CreateBox("k", {
      width: kw,
      height: kh,
      depth: kz,
    });
    kb.position.x = kw / 2;
    kb.position.y = kh / 2;
    kb.position.z = kz / 2;
    let _y = 0;
    kody.forEach((row) => {
      let _x = 0;
      row.forEach((col) => {
        if (!col)
          kb = makeHole(
            this.scene,
            kb,
            _x,
            kh - _y - config.text.block.size,
            0,
            config.text.block.size,
            config.text.block.size,
            kz
          );
        _x++;
      });
      _y++;
    });

    const bb = kb.getBoundingInfo().boundingBox.maximumWorld;
    back = addItem(
      this.scene,
      back,
      kb,
      (this.width - Number(bb.x)) / 2,
      2 * config.deck.thickness +
        config.bracket.size / 2 +
        config.print.tolerance.xy +
        Number(bb.z),
      config.deck.thickness / 2
    );

    let poutre;
    // bottom right
    poutre = MeshBuilder.CreateBox(
      "poutre",
      {
        width: this.bracket_size,
        height: this.bracket_size,
        depth: this.depth - config.deck.thickness,
      },
      this.scene
    );
    poutre.position.x = this.bracket_size / 2;
    poutre.position.y = this.bracket_size / 2;
    poutre.position.z = (this.depth - config.deck.thickness) / 2;
    back = addItem(
      this.scene,
      back,
      poutre,
      2 * config.deck.thickness +
        config.print.tolerance.xy +
        config.print.tolerance.xy +
        config.print.tolerance.xy +
        config.bracket.size / 2,
      2 * config.deck.thickness +
        config.print.tolerance.xy +
        config.print.tolerance.xy +
        config.print.tolerance.xy +
        config.bracket.size / 2,
      config.deck.thickness
    );
    // bottom left
    poutre = MeshBuilder.CreateBox(
      "poutre",
      {
        width: this.bracket_size,
        height: this.bracket_size,
        depth: this.depth - config.deck.thickness,
      },
      this.scene
    );
    poutre.position.x = this.bracket_size / 2;
    poutre.position.y = this.bracket_size / 2;
    poutre.position.z = (this.depth - config.deck.thickness) / 2;
    back = addItem(
      this.scene,
      back,
      poutre,
      this.width -
        (2 * config.deck.thickness +
          config.print.tolerance.xy +
          config.bracket.size +
          config.bracket.size / 2),
      2 * config.deck.thickness +
        config.print.tolerance.xy +
        config.print.tolerance.xy +
        config.print.tolerance.xy +
        config.bracket.size / 2,
      config.deck.thickness
    );
    // top right
    poutre = MeshBuilder.CreateBox(
      "poutre",
      {
        width: this.bracket_size,
        height: this.bracket_size,
        depth: this.depth - config.deck.thickness,
      },
      this.scene
    );
    poutre.position.x = this.bracket_size / 2;
    poutre.position.y = this.bracket_size / 2;
    poutre.position.z = (this.depth - config.deck.thickness) / 2;
    back = addItem(
      this.scene,
      back,
      poutre,
      2 * config.deck.thickness +
        config.print.tolerance.xy +
        config.print.tolerance.xy +
        config.print.tolerance.xy +
        config.bracket.size / 2,
      this.height -
        (2 * config.deck.thickness +
          config.print.tolerance.xy +
          config.bracket.size * 2) -
        config.bracket.size / 2,
      config.deck.thickness
    );
    // top left
    poutre = MeshBuilder.CreateBox(
      "poutre",
      {
        width: this.bracket_size,
        height: this.bracket_size,
        depth: this.depth - config.deck.thickness,
      },
      this.scene
    );
    poutre.position.x = this.bracket_size / 2;
    poutre.position.y = this.bracket_size / 2;
    poutre.position.z = (this.depth - config.deck.thickness) / 2;
    back = addItem(
      this.scene,
      back,
      poutre,
      this.width -
        (2 * config.deck.thickness +
          config.print.tolerance.xy +
          config.bracket.size +
          config.bracket.size / 2),
      this.height -
        (2 * config.deck.thickness +
          config.print.tolerance.xy +
          config.bracket.size) -
        config.text.block.size * 3,
      config.deck.thickness
    );
    back.material = this.material;
    back.translate(Axis.X, -this.width / 2, window.scene);
    back.translate(Axis.Y, -this.height / 2, window.scene);
    return back;
  }
}
