import { StandardMaterial, MeshBuilder, Color3, Axis } from "babylonjs";
import { makeHole, addItem } from "./Tools";
import { MeshWriter } from "meshwriter";

import { config } from "../assets/json/config.json";

export default class DeckFront {
  constructor(scene, nb, color, name) {
    this.offset = 2 * 2 * (config.deck.thickness + config.print.tolerance.xy);
    this.width = config.card.width + this.offset;
    this.height = config.card.height + this.offset;
    this.cardDepth = config.card.depth;
    this.nbCards = nb;
    this.name = name;
    this.depth =
      (this.cardDepth + config.print.tolerance.z) * this.nbCards +
      config.deck.thickness +
      config.deck.thickness;
    this.scene = scene;
    this.material = new StandardMaterial("std", this.scene);
    this.material.diffuseColor = Color3.FromHexString(color);
  }

  render3D() {
    let front = MeshBuilder.CreateBox(
      "front",
      {
        width: this.width,
        height: this.height,
        depth: this.depth,
      },
      this.scene
    );
    front.position.x = this.width / 2;
    front.position.y = this.height / 2;
    front.position.z = this.depth / 2;

    front = makeHole(
      this.scene,
      front,
      config.deck.thickness,
      config.deck.thickness,
      0,
      this.width - 2 * config.deck.thickness,
      this.height - 2 * config.deck.thickness,
      this.depth - config.deck.thickness
    );

    let w = config.deck.corner * config.deck.thickness;
    front = makeHole(
      this.scene,
      front,
      w,
      0,
      0,
      this.width - w * 2,
      this.height,
      this.depth - config.deck.thickness
    );
    front = makeHole(
      this.scene,
      front,
      0,
      w,
      0,
      this.width,
      this.height - w * 2,
      this.depth - config.deck.thickness
    );

    // code hole
    let code_hole_width =
      config.code.hole.size * config.code.nb.x + config.code.nb.x;
    let code_hole_height =
      config.code.hole.size * config.code.nb.y + config.code.nb.y;
    front = makeHole(
      this.scene,
      front,
      (this.width - code_hole_width) / 2,
      config.deck.thickness +
        config.deck.thickness +
        config.bracket.size / 2 +
        config.bracket.size / 2 +
        config.bracket.size,
      0,
      code_hole_width,
      code_hole_height,
      this.depth
    );

    let textarea_width =
      config.card.width -
      2 *
        (config.bracket.size +
          config.bracket.size / 2 +
          2 * config.text.block.size);
    front = makeHole(
      this.scene,
      front,
      (this.width - textarea_width) / 2,
      2 * config.deck.thickness +
        config.bracket.size / 2 +
        config.print.tolerance.xy,
      this.depth - config.deck.thickness,
      textarea_width,
      config.bracket.size -
        2 * (config.print.tolerance.xy + config.print.tolerance.xy),
      config.deck.thickness
    );

    let textarea_height = config.bracket.size * 2;
    front = makeHole(
      this.scene,
      front,
      (this.width - textarea_width) / 2,
      this.height -
        config.bracket.size / 2 -
        config.deck.thickness -
        textarea_height,
      this.depth - config.deck.thickness / 2,
      textarea_width,
      config.bracket.size * 2,
      config.deck.thickness / 2
    );

    // text
    const Writer = MeshWriter(this.scene);
    const text = new Writer(this.name.replace("_", " "), {
      "letter-height": 6,
      "letter-thickness": config.deck.thickness / 2,
      "font-family": "comic",
      color: this.color,
    });
    const textMesh = text.getMesh();
    const bb = textMesh.getBoundingInfo().boundingBox.maximumWorld;
    textMesh.rotation.x = -Math.PI / 2;
    front = addItem(
      this.scene,
      front,
      textMesh,
      (this.width - Number(bb.x)) / 2,
      this.height -
        config.bracket.size / 2 -
        config.deck.thickness -
        textarea_height +
        (textarea_height - Number(bb.z)) / 2,
      this.depth - config.deck.thickness / 2
    );

    front.material = this.material;
    front.translate(Axis.X, -this.width / 2, this.scene);
    front.translate(Axis.Y, -this.height / 2, this.scene);
    return front;
  }
}
