import { MeshBuilder, CSG } from "babylonjs";

function _merge(scene, mesh1, mesh2, type, debug = false) {
  const CSG1 = CSG.FromMesh(mesh1);
  const CSG2 = CSG.FromMesh(mesh2);
  let pipeCSG;
  if ("sub" == type) pipeCSG = CSG1.subtract(CSG2);
  else if ("add" == type) pipeCSG = CSG1.union(CSG2);
  let mesh = pipeCSG.toMesh("card", null, scene);
  mesh1.dispose();
  if (!debug) mesh2.dispose();
  scene.removeMesh(mesh1);
  if (!debug) scene.removeMesh(mesh2);
  return mesh;
}

export function makeHole(scene, card, x, y, z, w, h, d) {
  let hole = MeshBuilder.CreateBox(
    "hole",
    {
      width: w,
      height: h,
      depth: d,
    },
    scene
  );
  hole.position.x = w / 2 + x;
  hole.position.y = h / 2 + y;
  hole.position.z = d / 2 + z;
  return _merge(scene, card, hole, "sub");
}

export function addItem(scene, card, item, x, y, z, debug = false) {
  item.position.x += x;
  item.position.y += y;
  item.position.z += z;
  return _merge(scene, card, item, "add", debug);
}
